package com.vvitguntur.coffeeday;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    int a, b;
    TextView tv, tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tv = findViewById(R.id.text);
        tv1 = findViewById(R.id.text2);
    }

    public void add(View view) {
        a++;
        tv.setText("" + a);


    }

    public void bill(View view) {
        if (a >= 0) {
            b = a * 50;
            tv1.setText("" + b);
        } else {
            tv1.setText("" + 0);
        }

    }

    public void decre(View view) {

        if (a > 0) {
            a--;
            tv.setText("" + a);
        }
    }

    public void pay(View view) {
        a=0;
        b=0;
        tv.setText(""+a);
        tv1.setText(""+b);
        Toast.makeText(this, "Thank You...!", Toast.LENGTH_SHORT).show();
    }
}